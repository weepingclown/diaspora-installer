# This is required if updating from a version before 0.6.3.0
test "$(getent passwd diaspora | cut -d: -f6)" = "/var/lib/$user"  ||\
 usermod -d /var/lib/diaspora diaspora
mkdir -p /var/lib/diaspora

for i in public .bundle Gemfile.lock
do
  if [ -L /usr/share/diaspora/$i ]; then
    echo -n ""
  elif [ -d /usr/share/diaspora/$i ]; then
    mv /usr/share/diaspora/$i /var/lib/diaspora
    ln -sf /var/lib/diaspora/$i /usr/share/diaspora/$i
  fi
  mkdir -p /var/lib/diaspora/$i
done

if [ -L /usr/share/diaspora/app/assets ]; then
  echo -n ""
elif [ -d /usr/share/diaspora/app/assets ]; then
  mkdir -p /var/lib/diaspora/app-assets
  ln -sf /var/lib/diaspora/app-assets /usr/share/diaspora/app/assets
fi
mkdir -p /var/lib/diaspora/app-assets

if [ -L /usr/share/diaspora/vendor/bundle ]; then
  echo -n ""
elif [ -d /usr/share/diaspora/vendor/bundle ]; then
  mv /usr/share/diaspora/vendor/bundle /var/lib/diaspora/vendor-bundle
  ln -sf /var/lib/diaspora/vendor-bundle /usr/share/diaspora/vendor/bundle
fi
mkdir -p /var/lib/diaspora/vendor-bundle

mkdir -p /run/diaspora
if [ -L /usr/share/diaspora/tmp ]; then
  echo -n ""
elif [ -d /usr/share/diaspora/tmp ]; then
  mv /usr/share/diaspora/tmp/* /run/diaspora/
  rmdir /usr/share/diaspora/tmp/
  ln -sf /run/diaspora /usr/share/diaspora/tmp
fi
mkdir -p /run/diaspora/pids

mkdir -p /var/log/diaspora
if [ -L /usr/share/diaspora/log ]; then
  echo -n ""
elif [ -d /usr/share/diaspora/log ]; then
  mv /usr/share/diaspora/log/* /var/log/diaspora/
  rm -rf /usr/share/diaspora/log
  ln -sf /var/log/diaspora /usr/share/diaspora/log
fi

if [ -L /usr/share/diaspora/db/schema.rb ]; then
  echo -n ""
elif [ -f /usr/share/diaspora/db/schema.rb ]; then
  mv /usr/share/diaspora/db/schema.rb /var/lib/diaspora/db-schema.rb
  ln -sf /var/lib/diaspora/db-schema.rb /usr/share/diaspora/db/schema.rb
fi

chown diaspora: -R /var/lib/diaspora
chown diaspora: -R /var/log/diaspora
chown diaspora:www-data -R /run/diaspora
