diaspora-installer (0.7.18.2+debian) experimental; urgency=medium

  * specify 0.7.18.2 as version and add its sha256sum
  * add patch to use ruby 3 (Closes: #1006547)
  * stop fixing permission issues as diaspora user
  * mv diaspora.{yml,toml} to match upstream
  * enable cetificate authority
  * config bundler settings instead of passing flags to it
  * add patch to fix i18n translation issues
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * remove adpoted extended fields
  * update d/copyright and add myself to uploaders
  * remove ruby-interpreter depends and use ${ruby:Depends}
  * apply wrap-and-sort
  * add d/salsa-ci.yml

 -- Ananthu C V <weepingclown@disroot.org>  Sat, 13 Jan 2024 15:30:34 +0530

diaspora-installer (0.7.15.0+debian5) experimental; urgency=medium

  * Add certbot as a dependency for diaspora-common for let's encrypt support
  * Add set -e for better error handling and check for BUNDLE_WITH instead of DB

 -- Pirate Praveen <praveen@debian.org>  Mon, 27 Sep 2021 02:37:32 +0530

diaspora-installer (0.7.15.0+debian4) experimental; urgency=medium

  * Add certbot as a dependency for diaspora-common for let's encrypt support
   (Thanks to Rojin for testing and reporting this issue)

 -- Pirate Praveen <praveen@debian.org>  Fri, 02 Jul 2021 23:32:40 +0530

diaspora-installer (0.7.15.0+debian3) experimental; urgency=medium

  * Migrate /usr/share/diaspora/config as symbolic link

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Jun 2021 19:43:14 +0530

diaspora-installer (0.7.15.0+debian2) experimental; urgency=medium

  * Merge changes from master-sid branch

 -- Pirate Praveen <praveen@debian.org>  Mon, 28 Jun 2021 18:31:53 +0530

diaspora-installer (0.7.15.0+debian1) unstable; urgency=medium

  * Include changes from 0.7.14.0+debian2+nmu1 upload (Closes: #985336)
  * Include changelog entry from from 0.7.14.0+debian2+nmu2 (actual changes were
    part of 0.7.15 upstream release)

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 May 2021 22:27:14 +0530

diaspora-installer (0.7.15.0+debian) unstable; urgency=medium

  * Fix diaspora-sidekiq.service (make it part of gitlab.service)
  * Update README and add details of systemd service files
  * Mention debian specific documentation location at the end of installation
  * Install diaspora 0.7.15 (Closes: #986286)

 -- Pirate Praveen <praveen@debian.org>  Fri, 07 May 2021 14:13:07 +0530

diaspora-installer (0.7.14.0+debian2+nmu2) unstable; urgency=medium

  * Non-maintainer upload.
  * Adjust diaspora-download.sh to modify Gemfile.lock so mimemagic (0.3.10)
    is used (beside nokogiri (~> 1) and rake), upstream change 79133df
    (Closes: #986286)

 -- Jan Wagner <waja@cyconet.org>  Sun, 25 Apr 2021 23:33:26 +0200

diaspora-installer (0.7.14.0+debian2+nmu1) unstable; urgency=medium

  * Non-maintainer upload
  * Added missing dependency on tzdata (Closes: #985336)

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 08 Apr 2021 11:02:33 -0500

diaspora-installer (0.7.14.0+debian2) unstable; urgency=medium

  * Include changelog from experimental branch
  * Include systemd unit files (lower debhelper compat for old service file
    layout) (Closes: #887085)
  * Create /etc/diaspora/diaspora.conf instead of /etc/diaspora.conf for
    compatibility with systemd unit file
  * Migrate old diaspora.conf to new format
  * Use opentmpfiles for sysvinit support

 -- Pirate Praveen <praveen@debian.org>  Mon, 05 Oct 2020 22:33:21 +0530

diaspora-installer (0.7.14.0+debian1) experimental; urgency=medium

  [ Pirate Praveen ]
  * Add diaspora/config -> etc/diaspora symlink in diaspora-common
  * Add a maintscript to replace diaspora/config with symlink
  * Use diaspora user to manage download
  * Update permissions for diaspora user's home directory
  * Properly handle config symlink change
  * Add Breaks + Replaces for moving database.yml and diaspora.yml
  * Mention about symbolic links
  * Add patch for setting rails root to /usr/share/diaspora
  * Properly handle config symlink in postrm and preinst
  * Merge changes from master-sid branch

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Pirate Praveen <praveen@debian.org>  Fri, 18 Sep 2020 17:45:46 +0530

diaspora-installer (0.7.14.0+debian) unstable; urgency=medium

  * Update diaspora version to 0.7.14.0
  * Don't exclude Gemfile.lock (use upstream lock file for diaspora-installer)
  * Use --frozen option to bundle install to use upstream Gemfile.lock
  * Don't overiwrite config/oidc_key.pem during upgrades
  * Make config/schedule.yml writeable (Closes: #926968)

 -- Pirate Praveen <praveen@debian.org>  Wed, 09 Sep 2020 11:23:58 +0530

diaspora-installer (0.7.6.1+debian2) unstable; urgency=medium

  [ Lucas Kanashiro ]
  * d/t/control: add needs-internet restrictions

 -- Pirate Praveen <praveen@debian.org>  Tue, 05 May 2020 07:44:25 +0530

diaspora-installer (0.7.6.1+debian1) unstable; urgency=medium

  * Use system bundler (Closes: #919978)

 -- Pirate Praveen <praveen@debian.org>  Wed, 01 May 2019 18:45:53 +0530

diaspora-installer (0.7.6.1+debian) unstable; urgency=medium

  * Install bundler 1.x (Closes: #919978)

 -- Pirate Praveen <praveen@debian.org>  Fri, 26 Apr 2019 14:08:20 +0530

diaspora-installer (0.7.6.1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload

  [ Debconf translation updates ]
  * Portuguese (Américo Monteiro).  Closes: #852847
  * Dutch (Frans Spiesschaert).  Closes: #854626
  * Swedish (Jonatan Nyberg).  Closes: #855362
  * Italian (Beatrice Torracca). Closes: #860378
  * French (jean-pierre giraud).  Closes: #902492
  * German (Pfannenstein Erik).  Closes: #888785

 -- Helge Kreutzmann <debian@helgefjell.de>  Wed, 14 Nov 2018 13:54:44 +0100

diaspora-installer (0.7.6.0) unstable; urgency=medium

  * Update to 0.7.6.0, drop sigar dependency

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 03 Sep 2018 10:03:38 +0200

diaspora-installer (0.7.4.0+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch to libssl 1.1 (Closes: #894618)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 30 Aug 2018 10:59:22 +0200

diaspora-installer (0.7.4.0) unstable; urgency=medium

  [ Joseph Nuthalapati ]
  * New upstream release
  * Update debhelper compat version to 11
  * Update wiki url to use https
  * Modify VCS from anonscm to salsa
  * Change copyright format to https url

  [ Pirate Praveen ]
  * Add default-mta as a dependency

 -- Pirate Praveen <praveen@debian.org>  Tue, 27 Mar 2018 12:11:03 +0530

diaspora-installer (0.7.3.1+debian2) unstable; urgency=medium

  [ Balasankar C ]
  * Fix GitLab CI

  [ Pirate Praveen ]
  * Add zlib1g-dev dependency for diaspora-installer

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Feb 2018 13:16:02 +0530

diaspora-installer (0.7.3.1+debian1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Raju Devidas <rajudev@disroot.org>  Tue, 13 Feb 2018 01:38:06 +0530

diaspora-installer (0.7.2.0+debian5) unstable; urgency=medium

  * Add libffi-dev as dependency for diaspora-installer
  * Handle config/oidc_key.pem correctly for updates (this link was not created
    when updating from older version)

 -- Pirate Praveen <praveen@debian.org>  Fri, 09 Feb 2018 17:05:10 +0530

diaspora-installer (0.7.2.0+debian4) unstable; urgency=medium

  * export all variables in /etc/diaspora.conf (required for init script)
  * Update README and include recent changes

 -- Pirate Praveen <praveen@debian.org>  Sun, 14 Jan 2018 19:39:07 +0530

diaspora-installer (0.7.2.0+debian3) unstable; urgency=medium

  * Update check for existing database
    (new versions of PostgreSQL display a different error message now)
  * Run db:create instead of db:schema:load in new installations (db/schema.rb
    is removed now)
  * Thanks to Bady and Kannan for testing and feedback

 -- Pirate Praveen <praveen@debian.org>  Sat, 13 Jan 2018 17:59:51 +0530

diaspora-installer (0.7.2.0+debian2) unstable; urgency=medium

  * Remove bundler 1.16 support patch (0.7.2.0 already includes it)
  * Add symlink for oidc_key.pem
  * Fix ssl certificate file path

 -- Pirate Praveen <praveen@debian.org>  Thu, 11 Jan 2018 18:43:33 +0530

diaspora-installer (0.7.2.0+debian1) experimental; urgency=medium

  [ Raju Devidas ]
  * Initial work on 0.7.x support

  [ Pirate Praveen ]
  * Install 0.7.2.0 release

 -- Pirate Praveen <praveen@debian.org>  Wed, 10 Jan 2018 20:49:47 +0530

diaspora-installer (0.6.6.0+debian2) unstable; urgency=medium

  * Bump standards version
  * Apply patch for bundler 1.16 compatibility
  * Remove write permissions correctly (Closes: #866862)

 -- Pirate Praveen <praveen@debian.org>  Mon, 08 Jan 2018 20:40:24 +0530

diaspora-installer (0.6.6.0+debian1) unstable; urgency=medium

  * Install 0.6.6.0 version of diaspora
  * Set minimum version of ruby to 2.2
  * Create update checklist in README

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Jun 2017 11:03:15 +0530

diaspora-installer (0.6.5.0+debian1) unstable; urgency=medium

  * Install 0.6.5.0 version of diaspora
  * Create /run/diaspora for systems without systemd (Closes: #856720)
  * Read values from config file and seed debconf db
  * Configure nginx even when https disabled (in earlier versions, diaspora
    was accessible via port 3000, but now it is only available via unix socket)
  * Make postrm idempotent (don't fail if variable is not set)
  * Update values in config if changed via debconf
  * Ask letsencrypt update email
  * Change libssl-dev to libssl1.0-dev
  * Check sha256 sum of downloaded tarball

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Apr 2017 15:01:29 +0530

diaspora-installer (0.6.3.0+debian4) unstable; urgency=medium

  * Fix regression in diaspora-common.postrm which removes /bin by mistake

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Mar 2017 13:57:10 +0530

diaspora-installer (0.6.3.0+debian3) unstable; urgency=medium

  * Remove data during purge after user confirmation
  * Fix directory creation regression in preinst
  * Fix path for copying source.tar.gz
  * Correctly handle public directory in preinst backup
  * Clean diaspora-installer created files

 -- Pirate Praveen <praveen@debian.org>  Mon, 20 Mar 2017 18:45:32 +0530

diaspora-installer (0.6.3.0+debian2) unstable; urgency=medium

  * Change section to net (Closes: #832219)
  * Create public/source.tar.gz only if the file is missing
  * Fix diaspora backup logic for updates (to remove files removed upstream)
    (Closes: #856720)

 -- Pirate Praveen <praveen@debian.org>  Fri, 17 Mar 2017 11:43:04 +0530

diaspora-installer (0.6.3.0+debian1) unstable; urgency=medium

  * Install 0.6.3.0 version
  * Remove dbconfig files on purge (Closes: #850712)
  * Provide /var/log/diaspora symlink for /usr/share/diaspora/log
  * Change diaspora user home to /var/lib/diaspora (make /usr read-only)
  * Add symlinks in /usr/share/diaspora to /var/lib/diaspora for write access

  [ James Valleroy ]
  * Allow other webservers/make nginx optional (Closes: #812374)

 -- Pirate Praveen <praveen@debian.org>  Thu, 26 Jan 2017 04:39:32 +0530

diaspora-installer (0.6.0.0+debian6) unstable; urgency=medium

  * Add dbconfig-no-thanks as optional dependency (Closes: #812156)
  * Add reason for keeping this package in contrib (Closes: #785766)
  * Add www to servername in nginx.conf.example (Closes: #828858)
  * Portuguese translation updated by Américo Monteiro (Closes: #784585)
  * Spanish translation updated by jathan (Closes: #785637)
  * Swedish translation updated by Jonatan Nyberg (Closes: #822663)
  * Brazilian Portuguese translation updated by Adriano Rafael Gomes
    (Closes: #820253)
  * Dutch translation updated by Frans Spiesschaert (Closes: #799457)
  * Italian translation updated by Beatrice Torracca (Closes: #799135)
  * Danish translation updated by Joe Dalton (Closes: #787376)
  * French translation updated by Julien Patriarca (Closes: #786495)
  * Update Malayalam translation
  * Allow auto-configuration of SSL/TLS certificates with Let's Encrypt
  * Use /var/log/diaspora for logs in diaspora-installer too (Closes: #850860)
  * Update permissions of configuration files with secrets (Closes: #850858)

 -- Pirate Praveen <praveen@debian.org>  Fri, 13 Jan 2017 20:42:32 +0530

diaspora-installer (0.6.0.0+debian5) unstable; urgency=medium

  * diaspora-common: add lsb-base to depends
  * Update diaspora-common dependencies for mariadb (Closes: #848458)

  [ Balasankar C ]
  * Update diaspora-installer-mysql dependencies for mariadb (Closes: #848294)
  * Properly set permissions for gems installed by bundler (Closes: #847286)

 -- Pirate Praveen <praveen@debian.org>  Fri, 30 Dec 2016 19:32:06 +0530

diaspora-installer (0.6.0.0+debian4) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Wed, 09 Nov 2016 13:45:14 +0530

diaspora-installer (0.6.0.0+debian3) experimental; urgency=medium

  * Fix SERVERNAME variable creation
  * Remove database.yml on purge
  * Remove leftover files on uninstallation/purge
  * Use /etc/nginx/sites-available/diaspora as nginx conf file
  * Always run postinst (not just for updates)

 -- Pirate Praveen <praveen@debian.org>  Tue, 27 Sep 2016 19:08:05 +0530

diaspora-installer (0.6.0.0+debian2) experimental; urgency=medium

  * Update startscript.log path in init script

 -- Pirate Praveen <praveen@debian.org>  Mon, 26 Sep 2016 22:15:05 +0530

diaspora-installer (0.6.0.0+debian1) experimental; urgency=medium

  * Install 0.6.0.0 version
  * Make sure sigar build flags are set always
  * Use unix domain sockets in nginx
  * Cleanup postinst

 -- Pirate Praveen <praveen@debian.org>  Sat, 27 Aug 2016 12:05:03 +0530

diaspora-installer (0.5.9.1+debian5) unstable; urgency=medium

  * Move ca-certificates to diaspora-common Pre-Depends (autopkgtest fails
    otherwise because ca certificates are configured only after diaspora)
  * Move nginx.conf.example to /usr/share/diaspora-common (Closes: #834090)
  * Test only diaspora-installer (remove diaspora-installer-mysql install test)

 -- Pirate Praveen <praveen@debian.org>  Mon, 22 Aug 2016 22:16:30 +0530

diaspora-installer (0.5.9.1+debian4) unstable; urgency=medium

  * Move database.yml, diaspora.yml links to diaspora-installer binary
    (diaspora binary now links config to /etc/diaspora)

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jul 2016 19:14:01 +0530

diaspora-installer (0.5.9.1+debian3) unstable; urgency=medium

  * Run autopkgtest
  * Fix handling of diaspora.conf
  * Add ca-certificates to Pre-Depends (for downloading from github,
    Depends is not enough)

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jul 2016 14:34:25 +0530

diaspora-installer (0.5.9.1+debian2) unstable; urgency=medium

  * Install diaspora.conf and database.yml to /usr/share, copy to /var
    and modify (Closes: #820073)

 -- Pirate Praveen <praveen@debian.org>  Sun, 10 Jul 2016 11:48:41 +0530

diaspora-installer (0.5.9.1+debian1) unstable; urgency=medium

  * Install 0.5.9.1 version
  * Move private copy of diaspora.conf and database.yml to /var
    (Closes: #820073)

 -- Pirate Praveen <praveen@debian.org>  Sat, 02 Jul 2016 20:44:33 +0530

diaspora-installer (0.5.8.0+debian1) unstable; urgency=medium

  * Team upload.
  * Install 0.5.8.0 version.
  * Add a howto for updating the package - README.Debian.

 -- Balasankar C <balasankarc@autistici.org>  Mon, 04 Apr 2016 20:10:20 +0530

diaspora-installer (0.5.7.1+debian2) unstable; urgency=medium

  * Allow mysql as database (create new binary diaspora-installer-mysql)

 -- Pirate Praveen <praveen@debian.org>  Mon, 21 Mar 2016 14:52:53 +0530

diaspora-installer (0.5.7.1+debian1) unstable; urgency=medium

  [ Pirate Praveen ]
  * Install 0.5.7.1 version
  * Work around sigar bug (Closes: #818054)

  [ Cédric Boutillier ]
  * Run wrap-and-sort on packaging files
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Use https:// in Vcs-* fields
  * Remove version in the gem2deb build-dependency

  [ Balasankar C ]
  * Use ucf for handling conf files (Closes: #814621)

 -- Pirate Praveen <praveen@debian.org>  Sun, 13 Mar 2016 13:52:53 +0530

diaspora-installer (0.5.6.3+debian1) unstable; urgency=medium

  [ Balasankar C ]
  * Team upload.
  * Integrate third party service integration options to debconf.
  * Add postfix as alternative to exim4 and allow any mta
  * Switch dependency to dbconfig-pgsql from dbconfig-common

  [ Pirate Praveen ]
  * Install 0.5.6.3 version
  * diaspora-common: Add rake as dependency (Closes: #813271)

 -- Pirate Praveen <praveen@debian.org>  Mon, 08 Feb 2016 15:09:06 +0530

diaspora-installer (0.5.3.0+debian1) unstable; urgency=medium

  * Switch to upstream version 0.5.3.0
  * Set minimum version of rake to 10.4.2
  * Depend on same version of diaspora-common
  * diaspora-common:
    Remove dependency on rake and bundler (use bundle installed versions)

 -- Pirate Praveen <praveen@debian.org>  Mon, 07 Sep 2015 15:03:25 +0530

diaspora-installer (0.5.2.0+debian5) unstable; urgency=medium

  * diaspora-common.init:
    - Start diaspora with restart command even if not already running
    - Handle eye separately for diaspora and diaspora-installer
    - Check for running eye process before sending stop command

 -- Pirate Praveen <praveen@debian.org>  Sat, 29 Aug 2015 22:37:24 +0530

diaspora-installer (0.5.2.0+debian4) unstable; urgency=medium

  * Always remove bin symlink
  * Move diaspora.yml and database.yml to /etc to better handle configuration
    changes during upgrades
  * Run bundle exec for eye
  * Use eye for restart in init script

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Aug 2015 19:56:38 +0530

diaspora-installer (0.5.2.0+debian3) unstable; urgency=medium

  * Relax redis-server minimum version to 2:2.8
  * Install eye using rubygem for init script

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Aug 2015 13:08:23 +0530

diaspora-installer (0.5.2.0+debian2) unstable; urgency=medium

  * Use invoke-rc.d to start diaspora in postinst
  * Adapt init script for eye (monitoring tool)

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Aug 2015 00:35:16 +0530

diaspora-installer (0.5.2.0+debian1) unstable; urgency=medium

  * Use gem installed bundler
  * Support update from 0.4.x
  * Set minimum version of redis-server to 2:3.0
  * Update to 0.5.2.0 version of diaspora
  * Force gem installed bundler (symlink bin/bundle to /usr/local/bin/bundle)
  * Remove bin symlink to /usr/bin

 -- Pirate Praveen <praveen@debian.org>  Thu, 27 Aug 2015 19:39:30 +0530

diaspora-installer (0.5.0.1+debian2) unstable; urgency=medium

  * Provide diaspora.conf from diaspora-common (so it gets removed when purged)
  * Don't run postinstall if database exist already (run only migrations)
  * Add bc to diaspora-common dependencies
  * Move diaspora-common to main

 -- Pirate Praveen <praveen@debian.org>  Fri, 15 May 2015 09:28:30 +0530

diaspora-installer (0.5.0.1+debian1) unstable; urgency=medium

  * Make SSL configuration optional
    - disable nginx configuration when SSL is disabled
  * Provide database.yml and diaspora.yml from diaspora-common
  * Update diaspora-installer description (Thanks to  Justin B Rye)
  * Add -H to sudo to fix ruby-uuid error (Closes: #771544)
  * Bump diaspora version to 0.5.0.1

 -- Pirate Praveen <praveen@debian.org>  Fri, 08 May 2015 22:45:10 +0530

diaspora-installer (0.5.0.0+debian1) unstable; urgency=medium

  [Akshay S Dinesh]
  * Add a README file explaining the choices and steps (Closes: #781220)
  * Add mta | exim4 to depends (Ubuntu and derivates don't have a default mta)

  [Pirate Praveen]
  * Install README as doc
  * Needs bundler >= 1.7 to parse per gem source option in Gemfile
  * Switch to 0.5.0.0 release
  * Symlink /usr/bin to /usr/share/diaspora/bin
  * Move database configuration to diaspora-common
  * Add an introduction debconf note about postgresql password
    and ssl certificate
  * Add rake as dependency to diaspora-common
  * Remove dependencies needed only for building native gems from
    diaspora-common
  * Modify debconf tempaltes (Thanks to Justin B Rye)
  * Add debconf-updatepo to clean target of debian/rules

  * [Debconf translation updates]
  * Swedish (Martin Bagge / brother).  Closes: #781671
  * Czech (Michal Simunek).  Closes: #781799
  * Danish (Joe Hansen).  Closes: #781838
  * Dutch; (Frans Spiesschaert).  Closes: #781919
  * French (Julien Patriarca).  Closes: #782121
  * German (Erik Pfannenstein).  Closes: #782163
  * Russian (Yuri Kozlov).  Closes: #782263
  * Portuguese (Américo Monteiro).  Closes: #782547
  * Brazilian Portuguese (Adriano Rafael Gomes).  Closes: #782694
  * Italian (Beatrice Torracca).  Closes: #782800
  * Spanish (jathan). Closes: #783561
  * Malayalam (Pirate Praveen).

 -- Pirate Praveen <praveen@debian.org>  Wed, 06 May 2015 10:27:17 +0530

diaspora-installer (0.3) unstable; urgency=medium

  * Move to contrib (Closes: #779812)
  * Add wget to depends (Closes: #779868)
  * Update templates and descriptions (Closes: #779898)
    - Thanks to Christian Perrier and Justin B Rye for the review

 -- Pirate Praveen <praveen@debian.org>  Tue, 24 Mar 2015 09:42:13 +0530

diaspora-installer (0.2) unstable; urgency=medium

  * Initial release. (Closes: #771828)

 -- Pirate Praveen <praveen@debian.org>  Sun, 11 Jan 2015 18:45:25 +0530
